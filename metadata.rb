name 'elastic-beats'
maintainer 'Matthew Iverson'
maintainer_email 'matthewdiverson@gmail.com'
license 'MIT'
description 'Installs/Configures elastic-beats'
long_description 'Installs/Configures elastic-beats'
version '0.1.1'
chef_version '>= 14.0'

%w(centos fedora debian redhat ubuntu).each do |os|
  supports os
end

issues_url 'https://bitbucket.org/whonodes/elastic-beats/issues'
source_url 'https://bitbucket.org/whonodes/elastic-beats'
